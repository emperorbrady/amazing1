//package com.amazing.basetest.services;
//
//import java.math.BigInteger;
//
//import android.app.IntentService;
//import android.app.Service;
//import android.content.Intent;
//import android.os.IBinder;
//
//import com.amazing.basetest.helpers.Strings;
//
//public class ConversionService extends Service {
//	
//	
//	public ConversionService() {
//		super("Conversion Service");
//	}
//	
//	@Override
//	public IBinder onBind(Intent intent) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	
//	@Override
//	protected void onHandleIntent(Intent workIntent) {
//		String type = workIntent.getStringExtra("type");
//		String base1 = workIntent.getStringExtra("baseFrom");
//		String base2 = workIntent.getStringExtra("baseTo");
//		String num1 = workIntent.getStringExtra("numTo");
//		String num2 = workIntent.getStringExtra("numFrom");
//		
//		String val1 = "";
//		String val2 = "";
//		if(type.equals(Strings.BASE1CHANGE)) {
//			val1 = convert(num1,workIntent.getStringExtra("prevBase"),base1);
//			val2 = convert(num1,base1,base2);
//		} else if (type.equals(Strings.BASE2CHANGE)) {
//			val1 = convert(num2,workIntent.getStringExtra("prevBase"),base2);
//			val2 = convert(num2,base2,base1);
//		} else if (type.equals(Strings.NUM1CHANGE)) {
//			val1 = convert(num1,base1,base2);
//		} else if (type.equals(Strings.NUM2CHANGE)) {
//			val1 = convert(num2,base2,base1);
//		}
//		
//		sendResult(type, val1, val2);
//		
//		
//		
//    }
//	
//	public void sendResult(String message, String value1, String value2) {
//	    Intent intent = new Intent("com.amazing.basetest.UPDATEFIELD");
//	    String[] vals = {value1,value2};
//	    if(message != null) {
//	        intent.putExtra(message, vals);
//	    }
//	    sendBroadcast(intent);
//	}
//	
//	private String convert(String n1, String b1, String b2) {
//		int baseb1 = Integer.parseInt(b1);
//		int baseb2 = Integer.parseInt(b2);
//		return new BigInteger(n1, baseb1).toString(baseb2).toString();
//		
////		int num10 = 0;
////		String newNum = "";
////		//Convert to base ten
////		for(int i = 0; i < n1.length(); i++) {
////			int ith =  symbols.indexOf(n1.charAt(i));
////			num10 += Math.pow(baseb1,(n1.length() - i - 1)) * ith;
////		}
////		
////		//Convert to base b2
////		while(num10 > baseb2) {
////			newNum =  (num10 % baseb2) + newNum;
////			num10 /= baseb2;
////		}
////		
////		return newNum;
//	}
//
//	
//	
//}
